package views;

import models.Product;
import utils.Validator;

import java.util.Scanner;

public class SalesView {


    public String title;
    public int quantity;
    public double price;
    public Scanner scanner;
    public Product product;


    public SalesView(Product product) {
        this.product = product;
    }

    public void getInputs() {
        scanner = new Scanner(System.in);
        System.out.println
                (title = "Введите наименование товара: ");
        String name = Validator.validateName(scanner);
        product.setName(name);


        System.out.println
                (title = "Введите количество: ");
        quantity = Validator.validateQuantityInput(scanner);
        product.setQuantity(quantity);

        System.out.println
                (title = "Введите цену: ");
        price = Validator.validatePriceInput(scanner);
        product.setPrice(price);

        scanner.close();
    }

    public void getOutput(String output) {
        System.out.println(output);
    }
}
