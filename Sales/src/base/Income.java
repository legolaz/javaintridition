package base;



public interface Income {

    double profitCalculation(int quantityCount, double price);

    double profitCalculation(double profit);

}
