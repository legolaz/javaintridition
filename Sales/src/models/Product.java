package models;

import base.Fiscal;
import base.Income;


public class Product implements Fiscal, Income {


    private String name;
    private int quantity;
    private double price;

    private final int TAX_RATE = 5;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    @Override
    public double profitCalculation(int quantity, double price) {
        return quantity * price;
    }

    @Override
    public double taxCalculation(double profit) {
        return profit * TAX_RATE / 100;
    }


    @Override
    public double profitCalculation(double profit) {
        return (profit - TAX_RATE) - .2d;
    }
}
