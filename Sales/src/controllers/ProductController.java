package controllers;

import models.Product;
import utils.Rounder;
import views.SalesView;

import java.text.DecimalFormat;

public class ProductController {

    Product model;
    SalesView view;

    public ProductController(Product model, SalesView view) {
        this.model = model;
        this.view = view;
    }

    public void runApp() {

        view.getInputs();

        String name = model.getName();
        double totalProfit = model.profitCalculation(model.getQuantity(), model.getPrice());
        double totalProfitAfterPounding = Rounder.roundValue(totalProfit);
        double netProfit = model.profitCalculation(totalProfit);
        double netProfitAfterRounding = Rounder.roundValue(netProfit);
        double duty = model.taxCalculation(totalProfit);
        double dutyAfterPounding = Rounder.roundValue(duty);

        DecimalFormat formatter = new DecimalFormat("#.00");
        String output = "Имя Товара -> " + name + "\n" +
                "Общий доход (грн.) -> " + formatter.format(totalProfitAfterPounding) + "\n" +
                "Сумма налога (грн.) -> " + formatter.format(dutyAfterPounding) + "\n" +
                "Чистый доход (грн.) -> " + formatter.format(netProfitAfterRounding);

        view.getOutput(output);
    }
}
